var popup = document.querySelector(".popup_background");
var button = document.getElementsByTagName("button")[0];
var send = document.getElementsByTagName("button")[1];

[].forEach.call(document.getElementsByTagName("input"), function (item) {
    item.value = localStorage.getItem(item.name);
});

button.addEventListener("click", function () {
    "use strict";
    popup.classList.remove("hidden");
    history.pushState(null, null, "popup.html");
});

window.addEventListener("popstate", function () {
    "use strict";
    if (!popup.classList.contains("hidden")) {
        popup.classList.add("hidden");
        history.pushState(null, null, "index.html");
    }
});

send.addEventListener("click", function () {
    "use strict";
    localStorage.clear();
    history.pushState(null, null, "index.html");
    var letter = {
        name: document.getElementsByName("name"),
        email: document.getElementsByName("email"),
        message: document.getElementsByName("message"),
        check: document.getElementsByName("check")
    };
    fetch("https://formcarry.com/s/bGRnI5D5Ct", {
        method: "post",
        headers: {
            "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
        },
        body: JSON.stringify(letter)
    });
});

[].forEach.call(document.getElementsByTagName("input"), function (item) {
    item.addEventListener("change", function () {
        localStorage.setItem(item.name, item.value);
    });
});
